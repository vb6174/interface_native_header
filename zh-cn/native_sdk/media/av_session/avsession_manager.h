/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OHOS_AVSESSION_MANAGER_H
#define OHOS_AVSESSION_MANAGER_H

/**
 * @addtogroup avsession
 * @{
 *
 * @brief 音视频媒体会话，提供系统内媒体的统一控制能力。
 *
 * 功能包括媒体会话，媒体会话管理，媒体会话控制。
 *
 * @syscap SystemCapability.Multimedia.AVSession.Core
 * @since 9
 * @version 1.0
 */

/**
 * @file avsession_manager.h
 *
 * @brief 定义了会话管理器对外接口的功能的实现。
 *
 * @since 9
 * @version 1.0
 */

#include <functional>
#include <string>
#include <memory>

#include "audio_system_manager.h"
#include "av_session.h"
#include "avsession_controller.h"
#include "avsession_info.h"
#include "key_event.h"

namespace OHOS::AVSession {
/**
 * @brief 实现会话管理器对外接口功能的类的实现。
 *
 * @since 9
 * @version 1.0
 */
class AVSessionManager {
public:
    /**
     * @brief 获取会话管理器实例。
     *
     * @return 返回会话管理器实例。
     * @since 9
     * @version 1.0
     */
    static AVSessionManager& GetInstance();

    /**
     * @brief 创建AVSession会话的接口方法。
     *
     * @param tag AVSession的会话标签，不可为空。
     * @param type AVSession的会话类型,
     * 入参为{@link AVSession#SESSION_TYPE_AUDIO}，{@link AVSession#SESSION_TYPE_VIDEO}。
     * @param elementName  AVSession的会话名称{@link AppExecFwk::ElementName}。
     * @return 返回已创建的会话对象的智能指针。
     * @since 9
     * @version 1.0
     */
    virtual std::shared_ptr<AVSession> CreateSession(const std::string& tag, int32_t type,
                                                     const AppExecFwk::ElementName& elementName) = 0;

    /**
     * @brief 获取AVSession全部的会话描述的接口方法。
     *
     * @return 返回已存在的会话描述对象的数组{@link AVSessionDescriptor}。
     * @since 9
     * @version 1.0
     */
    virtual std::vector<AVSessionDescriptor> GetAllSessionDescriptors() = 0;

    /**
     * @brief 获取AVSession全部处于活动状态的会话描述的接口方法。
     *
     * @return 返回已存在的处于活动状态的会话描述对象的数组{@link AVSessionDescriptor}。
     * @since 9
     * @version 1.0
     */
    virtual std::vector<AVSessionDescriptor> GetActivatedSessionDescriptors() = 0;

    /**
     * @brief 通过会话ID获得会话描述的方法。
     *
     * @param sessionId AVSession的会话标签。
     * @param descriptor AVSession的会话类型，类型为{@link AVSessionDescriptor}。
     * @return 成功返回{@link #AVSESSION_SUCCESS}；失败则返回对应错误码。
     * @since 9
     * @version 1.0
     */
    virtual int32_t GetSessionDescriptorsBySessionId(const std::string& sessionId, AVSessionDescriptor& descriptor) = 0;

    /**
     * @brief 创建AVSession会话控制器的接口方法。
     *
     * @param sessionld AVSession的会话ID。
	 * @param controller 会话控制器实例，类型为{@link AVSessionController}。
     * @return 返回成功返回{@link #AVSESSION_SUCCESS}；失败则返回对应错误码。
     * @since 9
     * @version 1.0
     */
    virtual int32_t CreateController(const std::string& sessionId,
        std::shared_ptr<AVSessionController>& controller) = 0;

    /**
     * @brief 注册AVSession会话监听器的接口方法。
     *
     * @param listener 会话监听器的智能指针，类型为{@link SessionListener}。
     * @return 成功返回{@link #AVSESSION_SUCCESS}；失败则返回对应错误码。
     * @since 9
     * @version 1.0
     */
    virtual int32_t RegisterSessionListener(const std::shared_ptr<SessionListener>& listener) = 0;

    /**
     * @brief 注册AVSession服务器的死亡回调的接口方法。
     *
     * @param callback 死亡回调的方法，类型为{@link DeathCallback}。
     * @return 成功返回{@link #AVSESSION_SUCCESS}；失败则返回对应错误码。
     * @see UnregisterServiceDeathCallback
     * @since 9
     * @version 1.0
     */
    virtual int32_t RegisterServiceDeathCallback(const DeathCallback& callback) = 0;

    /**
     * @brief 注销AVSession服务器的死亡回调的接口方法。
     *
     * @return 成功返回{@link #AVSESSION_SUCCESS}；失败则返回对应错误码。
     * @see RegisterServiceDeathCallback
     * @since 9
     * @version 1.0
     */
    
    virtual int32_t UnregisterServiceDeathCallback() = 0;

    /**
     * @brief 发送系统按键事件的接口方法。
     *
     * @param keyEvent 按键事件码，类型为{@link MMI::KeyEvent}。
     * @return 成功返回{@link #AVSESSION_SUCCESS}；失败则返回对应错误码。
     * @since 9
     * @version 1.0
     */
    virtual int32_t SendSystemAVKeyEvent(const MMI::KeyEvent& keyEvent) = 0;

    /**
     * @brief 发送系统控制命令的接口方法。
     *
     * @param command 系统控制命令{@link AVControlCommand}。
     * @return 成功返回{@link #AVSESSION_SUCCESS}；失败则返回对应错误码。
     * @since 9
     * @version 1.0
     */
    virtual int32_t SendSystemControlCommand(const AVControlCommand& command) = 0;
    
    /**
     * @brief 将媒体会话投射到远程设备或投射回本地设备。
     *
     * @param token 需要投播的会话令牌。
     * @param descriptors 指定要转换的音频设备。
     * @return 成功返回{@link #AVSESSION_SUCCESS}；失败则返回对应错误码。
     * @since 9
     * @version 1.0
     */
    virtual int32_t CastAudio(const SessionToken& token,
                              const std::vector<AudioStandard::AudioDeviceDescriptor>& descriptors) = 0;
    
     /**
     * @brief 将此设备的所有媒体会话投播到远程设备。
     *
     * @param descriptors 指定要转换的音频设备。
     * @return 成功返回{@link #AVSESSION_SUCCESS}；失败则返回对应错误码。
     * @since 9
     * @version 1.0
     */
    virtual int32_t CastAudioForAll(const std::vector<AudioStandard::AudioDeviceDescriptor>& descriptors) = 0;
};
} // namespace OHOS::AVSession
/** @} */
#endif // OHOS_AVSESSION_MANAGER_H