/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OHOS_AVSESSION_CONTROLLER_H
#define OHOS_AVSESSION_CONTROLLER_H

/**
 * @addtogroup avsession
 * @{
 *
 * @brief 音视频媒体会话，提供系统内媒体的统一控制能力。
 *
 * 功能包括媒体会话，媒体会话管理，媒体会话控制。
 *
 * @syscap SystemCapability.Multimedia.AVSession.Core
 * @since 9
 * @version 1.0
 */

/**
 * @file avsession_controller.h
 *
 * @brief 控制器对象的描述，可获取会话的播放状态和会话元数据，远程发送控制命令到会话端也可以通过订阅监听会话段的更新事件。
 *
 * @since 9
 * @version 1.0
 */

#include <bitset>
#include <memory>
#include <string>
#include <vector>

#include "avcontrol_command.h"
#include "avsession_info.h"
#include "key_event.h"
#include "want_agent.h"

namespace OHOS::AVSession {
/**
 * @brief 控制器对象，可获取会话的播放状态和会话元数据，远程发送控制命令到会话端也可以通过订阅监听会话段的更新事件。
 */
class AVSessionController {
public:

    /**
     * @brief 获取音视频的播放状态。
     *
     * @param state 音视频的播放状态{@link AVPlaybackState}。
     * @return 成功返回{@link #AVSESSION_SUCCESS}；失败则返回对应错误码。
     * @see AVSession#SetAVPlaybackState
     * @see AVSession#GetAVPlaybackState
     * @since 9
     * @version 1.0
     */
    virtual int32_t GetAVPlaybackState(AVPlaybackState &state) = 0;

    /**
     * @brief 获取会话元数据。
     *
     * @param data 会话元数据{@link AVMetaData}。
     * @return 成功返回{@link #AVSESSION_SUCCESS}；失败则返回对应错误码。
     * @see AVSession#SetAVMetaData
     * @see AVSession#GetAVMetaData
     * @since 9
     * @version 1.0
     */
    virtual int32_t GetAVMetaData(AVMetaData &data) = 0;

    /**
     * @brief 发送系统按键事件。
     *
     * @param keyEvent 按键事件码，类型为{@link MMI::KeyEvent}。
     * @return 成功返回{@link #AVSESSION_SUCCESS}；失败则返回对应错误码。
     * @see AVSessionManager#SendSystemAVKeyEvent
     * @since 9
     * @version 1.0
     */
    virtual int32_t SendAVKeyEvent(const MMI::KeyEvent& keyEvent) = 0;

    /**
     * @brief 获取Ability。
     *
     * @param ability 类型为{@link AbilityRuntime::WantAgent::WantAgent}。
     * @return 成功返回{@link #AVSESSION_SUCCESS}；失败则返回对应错误码。
     * @see AVSession#SetLaunchAbility
     * @since 9
     * @version 1.0
     */
    virtual int32_t GetLaunchAbility(AbilityRuntime::WantAgent::WantAgent &ability) = 0;

    /**
     * @brief 获取媒体有效的指令。
     *
     * @param cmds 媒体有效的指令列表，范围为{@link #SESSION_CMD_INVALID}到{@link #SESSION_CMD_MAX}。
     * @return 成功返回{@link #AVSESSION_SUCCESS}；失败则返回对应错误码。
     * @see SendControlCommand
     * @see AVSession#AddSupportCommand
     * @see AVSession#DeleteSupportCommand
     * @since 9
     * @version 1.0
     */
    virtual int32_t GetValidCommands(std::vector<int32_t> &cmds) = 0;

    /**
     * @brief 获取session的激活状态。
     *
     * @param isActive session是否激活，类型为{@link AVSessionController}。
     * @return 成功返回{@link #AVSESSION_SUCCESS}；失败则返回对应错误码。
     * @see AVSession#Activate
     * @see AVSession#Deactivate
     * @see AVSession#IsActive
     * @since 9
     * @version 1.0
     */
    virtual int32_t IsSessionActive(bool &isActive) = 0;

    /**
     * @brief 发送媒体控制指令。
     *
     * @param cmd 媒体控制指令，类型为{@link AVControlCommand}。
     * @return 成功返回{@link #AVSESSION_SUCCESS}；失败则返回对应错误码。
     * @see GetValidCommands
     * @since 9
     * @version 1.0
     */
    virtual int32_t SendControlCommand(const AVControlCommand &cmd) = 0;

    /**
     * @brief 注册回调。
     *
     * @param callback 需要注册的回调，类型为{@link AVControllerCallback}。
     * @return 成功返回{@link #AVSESSION_SUCCESS}；失败则返回对应错误码。
     * @since 9
     * @version 1.0
     */
    virtual int32_t RegisterCallback(const std::shared_ptr<AVControllerCallback> &callback) = 0;

    /**
     * @brief 设置会话元数据过滤。
     *
     * @param filter 会话元数据过滤，类型为{@link AVMetaData#MetaMaskType}。
     * @return 成功返回{@link #AVSESSION_SUCCESS}；失败则返回对应错误码。
     * @since 9
     * @version 1.0
     */
    virtual int32_t SetMetaFilter(const AVMetaData::MetaMaskType &filter) = 0;

    /**
     * @brief 设置音视频的播放状态回调过滤。
     *
     * @param filter 音视频播放状态回调过滤，类型为{@link AVPlaybackState#PlaybackStateMaskType}。
     * @return 成功返回{@link #AVSESSION_SUCCESS}；失败则返回对应错误码。
     * @since 9
     * @version 1.0
     */
    virtual int32_t SetPlaybackFilter(const AVPlaybackState::PlaybackStateMaskType &filter) = 0;

    /**
     * @brief 释放控制器。
     *
     * @return 成功返回{@link #AVSESSION_SUCCESS}；失败则返回对应错误码。
     * @since 9
     * @version 1.0
     */
    virtual int32_t Destroy() = 0;

    /**
     * @brief 获取sessionId。
     *
     * @return 返回sessionId。
     * @since 9
     * @version 1.0
     */
    virtual std::string GetSessionId() = 0;

    /**
     * @brief 获取播放的实时位置，第 x ms。
     *
     * @return 返回播放的实时位置，第x ms，经过校正过的时间，单位为ms。
     * @since 9
     * @version 1.0
     */
    virtual int64_t GetRealPlaybackPosition() = 0;
    
    /**
     * @brief 是否释放控制器。
     *
     * @return true：释放；false：不释放。
     * @since 9
     * @version 1.0
     */
    virtual bool IsDestroy() = 0;

    virtual ~AVSessionController() = default;
};
} // namespace OHOS::AVSession
/** @} */
#endif // OHOS_AVSESSION_CONTROLLER_H