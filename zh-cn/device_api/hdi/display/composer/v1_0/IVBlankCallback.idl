/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Display
 * @{
 *
 * @brief 显示模块驱动接口定义。
 *
 * 提供给上层图形服务使用的驱动接口，包括图层管理、设备控制、显示内存管理等相关接口。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IVBlankCallback.idl
 *
 * @brief 帧同步事件回调接口声明。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief Display模块接口的包路径。
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.display.composer.v1_0;

import ohos.hdi.display.composer.v1_0.DisplayComposerType;

 /**
 * @brief 帧同步事件回调接口声明。
 *
 * @since 3.2
 * @version 1.0
 */

[callback] interface IVBlankCallback {
    OnVBlank([in] unsigned int sequence, [in] unsigned long ns);
}
/** @} */