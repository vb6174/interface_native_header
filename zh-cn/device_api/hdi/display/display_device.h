/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Display
 * @{
 *
 * @brief 显示模块驱动接口定义。
 *
 * 提供给图形系统使用的驱动接口，包括图层管理、设备控制、图形硬件加速、显示内存管理和回调接口等。
 * @since 1.0
 * @version 2.0
 */

 /**
 * @file display_device.h
 *
 * @brief 显示设备控制接口声明。
 *
 * @since 1.0
 * @version 2.0
 */

#ifndef DISPLAY_DEVICE_H
#define DISPLAY_DEVICE_H
#include "display_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 热插拔事件回调
 *
 * 当有热插拔事件发生时，会回调该接口，需要通过RegHotPlugCallback注册该回调接口。
 *
 * @paramdevId 输入参数，显示设备ID，此ID由HDI实现层生成，并通过当前回调接口传递给图形服务使用，用以标记当前连接的显示设备。
 * @param connected 输入参数，指明设备的连接状态，ture代表连接，false代表断开。
 * @param data 输出参数，图形服务携带的私有数据，该参数携带的是RegHotPlugCallback接口注册时传递的私有数据地址，
 * 详情参考 {@link RegHotPlugCallback} 接口注册时传递的地址。
 *
 * @since 1.0
 * @version 1.0
 */
typedef void (*HotPlugCallback)(uint32_t devId, bool connected, void *data);

/**
 * @brief VBlank 事件回调。
 *
 * 垂直同步事件回调接口，需要通过RegDisplayVBlankCallback注册该回调接口。
 *
 * @param sequence 输入参数，指明VBlank序列，是一个累加值。
 * @param ns 输入参数，该次VBlank事件的时间戳，以纳秒为单位。
 * @param data 输出参数，图形服务携带的私有数据，该参数携带的是RegDisplayVBlankCallback接口注册时传递的地址。
 *
 * @since 1.0
 * @version 1.0
 */
typedef void (*VBlankCallback)(unsigned int sequence, uint64_t ns, void *data);

/**
 * @brief 刷新请求回调
 *
 * 刷新请求回调，当接口实现层需要图形服务刷新数据帧时会回调该接口，需要通过RegDisplayRefreshCallback注册该回调接口。
 *
 * @param devId 输入参数，显示设备ID。
 * @param data 输出参数，图形服务携带的私有数据，该参数携带的是RegDisplayRefreshCallback接口注册时传递的地址。
 *
 * @since 1.0
 * @version 1.0
 */
typedef void (*RefreshCallback)(uint32_t devId, void *data);

/**
 * @brief 显示设备控制接口结构体，定义显示设备控制接口函数指针。
 */
typedef struct {
    /**
     * @brief 注册热插拔事件回调。
     *
     * 注册热插拔事件回调，当有热插拔事件发生时接口实现层需要回调注册的接口。
     *
     * @param callback 输入参数，热插拔事件回调实例，当有热插拔事件发生时,接口实现层需要通过该实例通知图形服务。
     * @param data 输出参数，图形服务携带的私有数据，在事件回调接口中需要返回给图形服务。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*RegHotPlugCallback)(HotPlugCallback callback, void *data);

    /**
     * @brief 注册VBlank事件回调。
     *
     * 注册VBlank事件回调，当有VBlank事件发生时接口实现层需要回调注册的接口。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     * @param callback 输入参数，VBlank事件回调实例，当有VBlank事件发生时并且DisplayVsync处于Enable状态,接口实现层需要通过该实例通知图形服务。
     * @param data 输出参数，图形服务携带的私有数据，在事件回调接口中需要返回给图形服务。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*RegDisplayVBlankCallback)(uint32_t devId, VBlankCallback callback, void *data);

    /**
     * @brief 刷新请求回调。
     *
     * 注册刷新请求事件回调，当实现层有刷新需求时，实现层通过回调注册的接口通知图形服务。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     * @param callback 输入参数，刷新请求事件回调实例，当有接口实现层有刷新请求时,需要通过该实例通知图形服务。
     * @param data 输出参数，图形服务携带的私有数据，在事件回调接口中需要返回给图形服务。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*RegDisplayRefreshCallback)(uint32_t devId, RefreshCallback callback, void *data);

    /**
     * @brief 获取显示设备能力集。
     *
     * 图形服务可以通过该接口获取显示设备具备哪些显示能力。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     * @param info 输出参数，设备支持的能力级信息，详情参考 {@link DisplayCapability}。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetDisplayCapability)(uint32_t devId, DisplayCapability *info);

    /**
     * @brief 获取显示设备支持的显示模式信息。
     *
     * 图形服务可以通过该接口获取到显示设备支持哪些显示模式。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     * @param num 输出参数，设备支持的模式数量。
     * @param modes 输出参数，设备支持的所有模式信息，包括所有能支持的分辨率和刷新率，每一个模式实现层都有一个Id与之对应，在获取当前模式
     * 和设置当前模式时都会使用到，详情参考 {@link DisplayModeInfo}。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetDisplaySupportedModes)(uint32_t devId, uint32_t *num, DisplayModeInfo *modes);

    /**
     * @brief 获取显示设备当前的显示模式。
     *
     * 图形服务可以通过该接口获取显示设备当前的显示模式。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     * @param modeId 输出参数，存放当前设备的显示模式ID， 由接口实现层进行数据的写入。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetDisplayMode)(uint32_t devId, uint32_t *modeId);

    /**
     * @brief 设置显示设备的显示模式。
     *
     * 图形服务可以通过该接口获设置显示设备的显示模式。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     * @param modeId 输入参数，指明需要设置的模式ID，接口实现层将设备切换到该参数对应的显示模式。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*SetDisplayMode)(uint32_t devId, uint32_t modeId);

    /**
     * @brief 获取显示设备当前的电源状态。
     *
     * 图形服务可以通过该接口获设置显示设备的电源状态。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     * @param status 输出参数，保存对应设备的电源状态，由接口实现层进行状态的写入。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetDisplayPowerStatus)(uint32_t devId, DispPowerStatus *status);

    /**
     * @brief 设置显示设备当前的电源状态。
     *
     * 图形服务可以通过该接口获设置显示设备的电源状态。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     * @param status 输入参数，表示需要设置的电源状态。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*SetDisplayPowerStatus)(uint32_t devId, DispPowerStatus status);

    /**
     * @brief 获取显示设备当前的背光值。
     *
     * 图形服务可以通过该接口获取设置显示设备的背光值。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     * @param level 输出参数，保存对应设备的背光值，由接口实现层进行写入。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetDisplayBacklight)(uint32_t devId, uint32_t *level);

    /**
     * @brief 设置显示设备当前的背光值。
     *
     * 图形服务可以通过该接口获设置显示设备的背光值。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     * @param level 输入参数，表示需要设置的背光值。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*SetDisplayBacklight)(uint32_t devId, uint32_t level);

    /**
     * @brief 获取显示设备属性值。
     *
     * 图形服务可以通过该接口获取显示设备具体的属性值。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     * @param id 输入参数，由接口GetDisplayCapability返回属性ID。
     * @param level 输出参数，属性ID对应的属性值，由接口实现层写入。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetDisplayProperty)(uint32_t devId, uint32_t id, uint64_t *value);

    /**
     * @brief 设置显示设备属性值。
     *
     * 图形服务可以通过该接口设置显示设备具体的属性值。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     * @param id 输入参数，由接口GetDisplayCapability返回属性ID。
     * @param value 输入参数，需要设置的属性值。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*SetDisplayProperty)(uint32_t devId, uint32_t id, uint64_t value);

     /**
     * @brief 显示设备合成前准备。
     *
     * 图形服务在合成前需要通过该接口通知显示设备进行合成前的准备工作。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     * @param needFlushFb 输出参数，接口实现层通过该参数指示图形服务在commit前是否需要通过SetDisplayClientBuffer重新设置显示帧存。
     * true表示需要设置显示帧存，false表示不需要。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*PrepareDisplayLayers)(uint32_t devId, bool *needFlushFb);

    /**
     * @brief 获取显示设备合成类型有变化的layer。
     *
     * 在合成准备阶段，显示设备会根据设备的合成能力修改图层的合成类型，该接口会返回哪些图层合成类型发生了变化。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     * @param num 输出参数，表示合成类型发生了变化的图层数量。
     * @param Layers 输出参数，指向图层数组首地址。
     * @param type 输出参数，指向合成类型数组首地址。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetDisplayCompChange)(uint32_t devId, uint32_t *num, uint32_t *Layers, int32_t *type);

    /**
     * @brief 设置显示设备的裁剪区域。
     *
     * 图形服务可以通过该接口设置显示设备的ClientBuffer的裁剪区域，裁剪区域不能超过ClientBuffer的大小。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     * @param rect 输入参数，ClientBuffer的裁剪区域。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*SetDisplayClientCrop)(uint32_t devId, IRect *rect);

    /**
     * @brief 设置显示设备的显示区域。
     *
     * 图形服务可以通过该接口设置显示设备的显示区域。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     * @param rect 输入参数，显示区域。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*SetDisplayClientDestRect)(uint32_t devId, IRect *rect);

    /**
     * @brief 设置显示设备的显示缓存。
     *
     * 图形服务可以通过该接口将显示缓存设置给显示设备，显示设备硬件合成结果将会存放在该显示缓存中。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     * @param buffer 输入参数，显示缓存。
     * @param fence 输入参数，同步栅栏，标识显示缓存是否可以访问，有图形服务创建和释放,接口实现层需要等待同步栅栏发送信号后才能使用显示缓存。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*SetDisplayClientBuffer)(uint32_t devId, const BufferHandle *buffer, int32_t fence);

    /**
     * @brief 设置显示设备的显示脏区。
     *
     * 图形服务可以通过该接口设置显示设备脏区，接口实现层可以根据该区域进行区域刷新，脏区是由多个矩形区域组合起来的。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     * @param num 输入参数，矩形个数。
     * @param rect 输入参数，区域矩形指针，指向矩形数组的首地址。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*SetDisplayClientDamage)(uint32_t devId, uint32_t num, IRect *rect);

    /**
     * @brief 使能垂直同步信号。
     *
     * 图形服务可以通过该接口使能或取消垂直同步信号，当有垂直同步信号产生时，接口实现层需要回调图形服务通过RegDisplayVBlankCallback注册的
     * VBlankCallback 回调。
     * 图形服务在需要刷新显示时需要使能垂直同步信号，在收到VBlankCallback事件回调时再进行合成送显,不需要刷新显示时需要取消垂直同步信号。
     * @param devId 输入参数，指示需要操作的设备ID。
     * @param enabled 输入参数，使能状态，true表示能，false表示不能。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*SetDisplayVsyncEnabled)(uint32_t devId, bool enabled);

    /**
     * @brief 获取显示图层fence。
     *
     * 图形服务在调用接口Commit后，需要通过该接口获取图层的fence信息。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     * @param num 输出参数，图层个数。
     * @param layers 输出参数，图层首地址，指向图层数组的首地址。
     * @param fences 输出参数，fence首地址，指向fence数组的首地址。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetDisplayReleaseFence)(uint32_t devId, uint32_t *num, uint32_t *layers, int32_t *fences);

    /**
     * @brief 获取显示设备支持的色域信息。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     * @param num 输出参数，设备支持的色域数组个数。
     * @param gamuts 输出参数，色域首地址，指向色域数组首地址。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetDisplaySupportedColorGamuts)(uint32_t devId, uint32_t *num, ColorGamut *gamuts);

    /**
     * @brief 获取显示设备当前的色域模式。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     * @param gamut 输出参数，保存对应设备的色域模式，由接口实现层进行写入。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetDisplayColorGamut)(uint32_t devId, ColorGamut *gamut);

    /**
     * @brief 设置显示设备当前的色域模式。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     * @param gamut 输入参数，表示需要设置的色域模式。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*SetDisplayColorGamut)(uint32_t devId, ColorGamut gamut);

    /**
     * @brief 获取显示设备当前的色域映射模式。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     * @param gamutMap 输出参数，保存对应设备的色域映射模式，由接口实现层进行写入。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetDisplayGamutMap)(uint32_t devId, GamutMap *gamutMap);

    /**
     * @brief 设置显示设备当前的色域映射模式。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     * @param gamutMap 输入参数，表示需要设置的色域映射模式。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*SetDisplayGamutMap)(uint32_t devId, GamutMap gamutMap);

    /**
     * @brief 设置显示设备当前的4x4的颜色转换矩阵。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     * @param matrix 输入参数，表示需要设置的颜色转换模式。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*SetDisplayColorTransform)(uint32_t devId, const float *matrix);

    /**
     * @brief 获取显示设备支持的HDR属性信息。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     * @param info 输出参数，保存对应设备的HDR属性信息，由接口实现层进行写入。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetHDRCapabilityInfos)(uint32_t devId, HDRCapability *info);

    /**
     * @brief 获取显示设备支持的 HDRMetadataKey。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     * @param num 输出参数，设备支持的HDRMetadataKey数组个数。
     * @param keys 输出参数，HDRMetadataKey首地址, 指向HDRMetadataKey数组首地址。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetSupportedMetadataKey)(uint32_t devId, uint32_t *num, HDRMetadataKey *keys);

    /**
     * @brief 提交合成送显请求。
     *
     * 图形服务通过该接口向接口实现层提交合成送显请求，如果有硬件合成层，接口实现层需要在这时进行合成，并且把最终合成的数据送到硬件进行显示。
     *
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     * @param fences 输出参数，fence首地址，指向fence数组的首地址。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*Commit)(uint32_t devId, int32_t *fence);

    /**
     * @brief 调用显示设备命令。
     *
     * 该接口用于图形服务和接口实现层之间的的接口扩展，如果由临时接口新增可以通过该接口进行扩展，不用增加新的接口定义。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*InvokeDisplayCmd)(uint32_t devId, ...);

    /**
     * @brief 创建虚拟显示设备。
     *
     * 该接口用于创建一个虚拟显示设备。
     *
     * @param width 输入参数，指定显示设备的像素宽度。
     * @param height 输入参数，指定显示设备的像素高度。
     * @param format 输出参数，指定显示设备的像素格式。
     * 详情参考{@link PixelFormat}，接口实现层可以根据硬件需求，修改format并返回给图形服务。
     * @param devId 输出参数，用于接口层返回创建的设备ID。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*CreateVirtualDisplay)(uint32_t width, uint32_t height, int32_t *format, uint32_t *devId);

    /**
     * @brief 销毁虚拟显示设备。
     *
     * 该接口用于销毁指定的虚拟显示设备。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*DestroyVirtualDisplay)(uint32_t devId);

    /**
     * @brief 设置虚拟屏的输出缓存。
     *
     * 该接口用于设置虚拟屏输出缓存，接口实现层需要将虚拟屏的输出放入到该缓存中，接口实现层需要等待同步栅栏发送信号后才能使用缓存。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     * @param buffer 输出参数，输出缓存。
     * @param fence 输出参数，同步栅栏。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*SetVirtualDisplayBuffer)(uint32_t devId, BufferHandle *buffer, int32_t fence);

    /**
     * @brief 获取显示设备的回写帧。
     *
     * 该接口用来获取devId指定的回写点数据，接口实现层将显示设备回写点的数据写入到设置的缓存中。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     * @param buffer 输出参数，回写点数据缓存。
     * @param fence 输出参数，同步栅栏，图形服务在调用该接口时，需要将Buffer的release fence
     * 传入，标记该缓存是否可以被写入，接口实现层在接口返回前需要将buffer的acquirce fence
     * 写入回传给图形服务，用来标记回写数据是否已经写入缓存。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetWriteBackFrame)(uint32_t devId, BufferHandle *buffer, int32_t *fence);

    /**
     * @brief 对指定显示设备创建回写点。
     *
     * 该接口用来对指定的设备创建一个回写点，如果回写点数量超过了限制，接口实现层将会返回失败。
     *
     * @param devId 输入参数，指示需要操作的设备ID，接口实现层在创建完回写点后将回写点设备ID存放在该参数中返回给图形服务。
     * @param width 输入参数，回写像素宽度。
     * @param height 输入参数，回写像素高度。
     * @param format 输入参数，回写点数据格式，详情参考{@}PixelFormat, 接口实现层可以根据硬件需求，修改format并返回给图形服务。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*CreateWriteBack)(uint32_t *devId, uint32_t width, uint32_t height, int32_t *format);

    /**
     * @brief 销毁指定显示设备的回写点。
     *
     * 该接口用來销毁指定的回写点。
     *
     * @param devId 输入参数，指示需要操作的设备ID。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*DestroyWriteBack)(uint32_t devId);
} DeviceFuncs;

/**
 * @brief 实现显示设备控制接口的初始化，申请操作显示设备控制接口的资源，并获取对应的操作接口。
 *
 * @param funcs 输出参数，显示设备控制接口指针，初始化时分配内存，调用者不需要分配内存，调用者获取该指针用于操作显示设备。
 *
 * @return DISPLAY_SUCCESS 表示执行成功。
 * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
 *
 * @since 1.0
 * @version 1.0
 */
int32_t DeviceInitialize(DeviceFuncs **funcs);

/**
 * @brief 取消显示设备控制接口的初始化，释放控制接口使用到的资源。
 *
 * @param 输出参数，显示设备控制接口指针，用于释放初始化函数中分配的操作指针内存。
 *
 * @return DISPLAY_SUCCESS 表示执行成功。
 * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
 *
 * @since 1.0
 * @version 1.0
 */
int32_t DeviceUninitialize(DeviceFuncs *funcs);

#ifdef __cplusplus
}
#endif
#endif
/* @} */