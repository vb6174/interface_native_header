/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup thermal
 * @{
 *
 * @brief 提供设备温度管理、控制及订阅接口。
 *
 * 热模块为热服务提供的设备温度管理、控制及订阅接口。
 * 服务获取此模块的对象或代理后，可以调用相关的接口管理、控制和订阅设备温度。
 *
 * @since 3.1
 * @version 1.0
 */

/**
 * @file IThermalInterface.idl
 *
 * @brief 设备温度管理、控制及订阅接口。
 *
 * 热模块为热服务提供的设备温度管理、控制及订阅接口。
 *
 * @since 3.1
 * @version 1.0
 */

package ohos.hdi.thermal.v1_0;

import ohos.hdi.thermal.v1_0.ThermalTypes;
import ohos.hdi.thermal.v1_0.IThermalCallback;

/**
 * @brief 设备温度管理、控制及订阅接口。
 *
 * 服务获取此对象后，可以调用相关的接口管理、控制和订阅设备温度。
 *
 * @since 3.1
 */
interface IThermalInterface {
    /**
     * @brief 设置CPU频率。
     *
     * @param freq 输入参数，设置CPU频率的值。
     *
     * @return HDF_SUCCESS 表示设置成功。
     *
     * @since 3.1
     */
    SetCpuFreq([in] int freq);

    /**
     * @brief 设置GPU频率。
     *
     * @param freq 输入参数，设置GPU频率的值。
     *
     * @return HDF_SUCCESS 表示设置成功。
     *
     * @since 3.1
     */
    SetGpuFreq([in] int freq);

    /**
     * @brief 设置充电电流。
     *
     * @param current 输入参数，充电电流，单位毫安。
     *
     * @return HDF_SUCCESS 表示设置成功
     *
     * @since 3.1
     */
    SetBatteryCurrent([in] int current);

    /**
     * @brief 获取设备发热的信息。
     *
     * @param event 输出参数，设备发热信息，包括器件类型、器件温度。
     *
     * @return HDF_SUCCESS 表示获取成功。
     * @see HdfThermalCallbackInfo
     *
     * @since 3.1
     */
    GetThermalZoneInfo([out] struct HdfThermalCallbackInfo event);

    /**
     * @brief 注册设备发热状态的回调。
     *
     * @param callbackObj 输入参数，服务注册的回调。
     *
     * @return HDF_SUCCESS 表示注册成功。
     * @see IThermalCallback
     *
     * @since 3.1
     */
    Register([in] IThermalCallback callbackObj);

    /**
     * @brief 取消注册设备发热状态的回调。
     *
     * @return HDF_SUCCESS 表示取消注册成功。
     *
     * @since 3.1
     */
    Unregister();
}
/** @} */
