/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdfPinAuth
 * @{
 *
 * @brief 提供口令认证驱动的标准API接口。
 *
 * 口令认证驱动为口令认证服务提供统一的访问接口。获取口令认证驱动代理后，口令认证服务可以调用相关接口获取执行器，获取口令认证执行器后，
 * 口令认证服务可以调用相关接口获取执行器信息，获取凭据模版信息，注册口令，认证口令，删除口令等。
 *
 * @since 3.2
 */

/**
 * @file IExecutor.idl
 *
 * @brief 定义执行器标准API接口。接口可用于获取执行器信息，获取凭据模版信息，注册口令，认证口令，删除口令等。
 *
 * @since 3.2
 */

package ohos.hdi.pin_auth.v1_0;

import ohos.hdi.pin_auth.v1_0.PinAuthTypes;
import ohos.hdi.pin_auth.v1_0.IExecutorCallback;

/**
 * @brief 定义执行器标准API接口。接口可用于获取执行器信息，获取凭据模版信息，注册口令，认证口令，删除口令等。
 *
 * @since 3.2
 * @version 1.0
 */

interface IExecutor {
    /**
     * @brief 获取执行器信息，口令认证服务将执行器注册到用户认证框架时需要通过该接口获取对应信息。
     *
     * @param executorInfo 执行器信息{@link ExecutorInfo}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    GetExecutorInfo([out] struct ExecutorInfo executorInfo);
    /**
     * @brief 获取凭据模版信息。
     *
     * @param templateId 凭据模版ID。
     * @param templateInfo 凭据模版信息{@link TemplateInfo}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    GetTemplateInfo([in] unsigned long templateId, [out] struct TemplateInfo templateInfo);
    /**
     * @brief 完成执行器注册，对口令模版信息进行对账，用于删除无效的口令模板及相关信息。
     *
     * @param templateIdList 用户认证框架内由该执行器注册的口令凭据模版ID列表。
     * @param frameworkPublicKey 用户认证框架的公钥，用于校验用户认证框架私钥签名的信息。
     * @param extraInfo 其他相关信息，用于支持信息扩展。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    OnRegisterFinish([in] unsigned long[] templateIdList, [in] unsigned char[] frameworkPublicKey, [in] unsigned char[] extraInfo);
    /**
     * @brief 设置口令数据，口令认证驱动处理注册或认证口令请求时，如果口令数据由口令认证服务获取，需要通过该接口将口令数据传给口令认证驱动。
     *
     * @param scheduleId 调度ID，用于标识一次操作请求的调度过程。
     * @param authSubType 口令子类型，如六位数字PIN码等。
     * @param data 口令数据。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    OnSetData([in] unsigned long scheduleId, [in] unsigned long authSubType, [in] unsigned char[] data);
    /**
     * @brief 注册口令。
     *
     * @param scheduleId 调度ID，用于标识一次操作请求的调度过程。
     * @param extraInfo 其他相关信息，用于支持信息扩展。
     * @param callbackObj 回调对象{@link IExecutorCallback}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    Enroll([in] unsigned long scheduleId, [in] unsigned char[] extraInfo, [in] IExecutorCallback callbackObj);
    /**
     * @brief 认证口令。
     *
     * @param scheduleId 调度ID，用于标识一次操作请求的调度过程。
     * @param templateId 指定要认证的模版ID。
     * @param extraInfo 其他相关信息，用于支持信息扩展。
     * @param callbackObj 回调对象{@link IExecutorCallback}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    Authenticate([in] unsigned long scheduleId, [in] unsigned long templateId, [in] unsigned char[] extraInfo, [in] IExecutorCallback callbackObj);
    /**
     * @brief 删除口令。
     *
     * @param templateId 模版ID。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    Delete([in] unsigned long templateId);
    /**
     * @brief 取消操作请求。
     *
     * @param scheduleId 调度ID，用于标识一次操作请求的调度过程。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    Cancel([in] unsigned long scheduleId);
    /**
     * @brief 发送口令认证功能相关操作命令。
     *
     * @param commandId 操作命令ID{@link CommandId}。
     * @param extraInfo 其他相关信息，用于支持信息扩展。
     * @param callbackObj 回调对象{@link IExecutorCallback}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    SendCommand([in] int commandId, [in] unsigned char[] extraInfo, [in] IExecutorCallback callbackObj);
}
/** @} */